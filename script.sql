/* * * * * * * * * * * * * * * * * * * * 
* Calculate the minimum time difference
* after each data insertion
* * * * * * * * * * * * * * * * * * * * */

WITH temp_table AS (
	SELECT  id, created_at FROM [TABLE]
) 
SELECT
    id,
	created_at, 
	LAG(created_at, 1) OVER (
		ORDER BY created_at
	) previous_create_at,
    created_at - LAG(created_at, 1) OVER (
		ORDER BY created_at
	) AS diff_time
FROM temp_table
ORDER BY diff_time;

/* * * * * * * * * * * * * * * * * * * * 
* Calculate the minimum time difference
* after each data insertion
* * * * * * * * * * * * * * * * * * * * */
UPDATE [TABLE]
SET [FOREIN_KEY] = sub_query."id"
FROM (
    SELECT 
    t1."id", t2."id" "t2Id"
    FROM [TABLE_1] t1
    LEFT JOIN [TABLE_2] t2
    ON t2."created_at" >= t1."created_at" AND t2."created_at" <= t1."created_at" + interval '2 minute' // Result Minimum Time for each insertion
) AS sub_query
WHERE public."t2"."id" = sub_query."t2Id"
RETURNING *;

